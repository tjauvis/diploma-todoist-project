# Vue 3 + Vite + Element Plus + Tailwind + Supabase / serverless

## Install dependencies
> yarn install

## Run project in dev mode
> yarn dev

## Build project for productions
> yarn build
