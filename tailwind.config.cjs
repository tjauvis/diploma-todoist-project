/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    container: {
      padding: '1rem',
      center: true,
      screens: {
        DEFAULT: '900px'
      }
    },
    extend: {
      fontFamily: {
        sans: ['Roboto', 'sans-serif']
      },
      colors: {
        primary: {
          DEFAULT: '#4AC2FB',
          orange: '#FB834A',
          'orange-dark-2': '#FA6118'
        },
        secondary: {
          DEFAULT: '#7CD3FC',
          orange: '#FCA57C',
          'orange-light-5': '#fcbba2'
        }
      }
    }
  },
  plugins: []
}
