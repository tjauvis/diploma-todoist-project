import { defineStore } from 'pinia'
import { api } from './api/api.js'

export const useStore = defineStore('store', {
  state: () => ({
    user: null
  }),

  actions: {
    async fetchUser() {
      const { data } = await api.auth.getCurrentUser()

      if (data) {
        this.user = data.user
      }
    },
    async logout() {
      await api.auth.logout()

      this.user = null
    }
  }
})
