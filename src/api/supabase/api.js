import todos from './services/todos.js'
import { supabase } from './instance.js'
import auth from './services/auth.js'

export const supabaseApi = {
  todos: todos(supabase),
  auth: auth(supabase)
}
