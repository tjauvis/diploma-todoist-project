/**
 * @param {import('@supabase/supabase-js').SupabaseClient} api
 * */
export default (api) => {
  const signUp = async (email, password) => {
    return api.auth.signUp({ email, password })
  }

  const signInWithPassword = async (email, password) => {
    return api.auth.signInWithPassword({ email, password })
  }

  const signInWithGoogle = async () => {
    return api.auth.signInWithOAuth({
      provider: 'google'
    })
  }

  const getCurrentUser = async () => {
    return api.auth.getUser()
  }

  const logout = async () => {
    return api.auth.signOut()
  }

  return {
    signUp,
    signInWithPassword,
    signInWithGoogle,
    getCurrentUser,
    logout
  }
}
