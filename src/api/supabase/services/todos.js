/**
 * @param {import('@supabase/supabase-js').SupabaseClient} api
 * */
export default (api) => {
  const getTasks = async (ascending = false, userId) => {
    let queryBuilder = api.from('todos').select()

    if (userId) {
      queryBuilder = queryBuilder.eq('user_id', userId)
    }

    return queryBuilder.order('created_at', { ascending })
  }

  /**
   * Update task
   * @param {string} id
   * @param {Object.<string,string>} data
   */
  const updateTodo = async (id, data) => {
    return api.from('todos').update(data).eq('id', id)
  }

  const create = async (data) => {
    return api.from('todos').insert(data).select()
  }

  const deleteTodo = async (id) => {
    return api.from('todos').delete().eq('id', id)
  }

  return {
    getTasks,
    updateTodo,
    create,
    deleteTodo
  }
}
