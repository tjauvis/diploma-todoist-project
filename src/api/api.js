import { supabaseApi } from './supabase/api.js'

export const api = {
  todos: supabaseApi.todos,
  auth: supabaseApi.auth
}
