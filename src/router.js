import { createRouter, createWebHistory } from 'vue-router'

/**
 * @type {import('vue-router').RouteRecordRaw[]}
 */
const routes = [
  {
    name: 'main',
    path: '/',
    component: () => import('./views/MainPage.vue')
  },
  {
    name: 'all-tasks',
    path: '/all',
    component: () => import('./views/MainPage.vue')
  },
  {
    name: 'login',
    path: '/login',
    component: () => import('./views/LoginPage.vue'),
    meta: {
      isUserFree: true
    }
  },
  {
    name: 'register',
    path: '/register',
    component: () => import('./views/RegisterPage.vue'),
    meta: {
      isUserFree: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export { router }
